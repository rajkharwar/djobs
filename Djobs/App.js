/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import SplashScreen from './app/components/SplashScreen';

import Signup from './app/components/Signup';
import SignIn from './app/components/SignIn';
import WelcomeOne from './app/components/WelcomeOne';
import WelcomeTwo from './app/components/WelcomeTwo';
import WelcomeThree from './app/components/WelcomeThree';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomePage from './app/components/HomePage';
import SignUpNavigation from './app/components/SignupNaviagtion';
import SignUp from './app/components/Signup';
import EnterOtp from './app/components/EnterOtp';
import WelcomeSwiper from './app/components/WelcomeSwiper';
import GoogleLocation from './app/components/GoogleLocation';
import HomeScrolls from './app/components/HomeScrolls';
import Profile from './app/components/Profile';
import PayScaleOne from './app/components/PayScaleOne';
import PayScaleTwo from './app/components/PayScaleTwo';
import PayScaleThree from './app/components/PayScaleThree';
import PayScaleFour from './app/components/PayScaleFour';
import PersonalDetail from './app/components/PersonalDetail';


const initialNavigator = createSwitchNavigator(
        {
          SplashScreen: SplashScreen,
          SignUpNavigation:SignUpNavigation,
          HomePage: HomePage,
            WelcomeSwiper:WelcomeSwiper,
            SignIn:SignIn,
        },
        {
          initialRouteName: 'SplashScreen',
        }

);
  const AppContainer = createAppContainer(initialNavigator);
class App extends React.Component {

  /*  constructor(props)
    {
        super(props);
        this.state = {
            signedIn:false,
        };
    }
    componentDidMount(): void {
        isSignedIn().then(res=>this.setState({signedIn:res}))
            .catch(err=>alert("an error occured"));
    }

    render() {
        const {  signedIn } = this.state;
        if(signedIn) {
            return <Home/>;
        }
        else {
            return <SignIn/>;
        }
    }*/
class App extends React.Component {
  render(){
    return(
        <PersonalDetail/>

    )
  }

}

export default App;
