import React, {Component} from 'react';
import { StyleSheet ,View,Text,Image,TouchableOpacity} from 'react-native';

export default class Profile extends React.Component{
    render(){
        return(
            <View>
                <View style={{flexDirection:'row',alignItems:'center',borderBottomColor: '#D8D8D8',
                    borderBottomWidth: 2,marginLeft:15,paddingBottom:20,paddingTop:30}}>
                <Image source={require('../images/profile.png')} style={{width:96,height:96,borderRadius:46}}/>
                       <View style={{flexDirection:'column',alignContent:'center',marginLeft:10}}>
                         <Text style={{fontSize:18,color:'#29275F'}}>Dharmendra Singh </Text>
                          <Text style={{fontSize:13,color:'#ABB2B9'}}>Dharmendra@gmail.com</Text>
                         <Text style={{fontSize:16,fontFamily:' Nunito Sans',color:'#29275F'}}>+918892652336</Text>
                       </View>

                </View>
                <View style={{flexDirection:'column',justifyContent:'space-between'}}>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/personal.png')}
                            style={{width:25,height:20}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Personal Details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/subscription.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Subscription Plan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/policy.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Policies</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/partner.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Partner With Us</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/support.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Customer Support</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/settings.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Setting</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',marginLeft:15,paddingTop:20}}>
                        <Image
                            source={require('../images/forgot.png')}
                            style={{width:25,height:25}}
                        />
                        <Text style={{fontSize:18,paddingLeft:27,fontFamily:'Nunito Sans',color:'#29275F'}}>Change Password</Text>
                    </TouchableOpacity>



                </View>
            </View>
        );
    }

}