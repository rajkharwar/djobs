import React, { Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button, StyleSheet, TouchableOpacity,
} from 'react-native';
import axios from 'axios';

export default class CreatePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {Password : "", ConfirmPassword : ""};
    }
    _onPressButton=()=> {
        if(this.state.Password == this.state.ConfirmPassword){
            axios.post('https://djobs.azurewebsites.net/api/Login/CreatePasword?ClientId='+1+'&Password='+this.state.Password)
                .then((response) => {
                    alert(response);
                }, (error) => {
                    console.log(error);
                });
        }
        else{
            alert('Password Not Match');
        }
    }

    render() {
        return (
//onPress={this._onPressButton}
        <View style={{paddingTop:10,paddingHorizontal:18,flex: 1}}>
            <Text style={styles.signup}>Create Password</Text>
            <Text style={styles.textlabel}>Password</Text>
            <TextInput style={styles.inputtext} secureTextEntry={true} onChangeText={(Password) => this.setState({Password})} value={this.state.Password}  placeholder = "Enter Password" underlineColorAndroid='transparent'/>
            <Text style={styles.textlabel}>Confirm Password</Text>
            <TextInput style={styles.inputtext} secureTextEntry={true}  onChangeText={(ConfirmPassword) => this.setState({ConfirmPassword})} value={this.state.ConfirmPassword}   placeholder = "Confirm Password" underlineColorAndroid='transparent'/>
            <TouchableOpacity onPress={this._onPressButton}  style={{marginTop:10,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#DDDDDD',paddingTop:20,borderRadius:10}}>
                <Text>Save And Continue</Text>
            </TouchableOpacity>
        </View>

        )
    }
}

const styles= StyleSheet.create(
    {

        inputtext:{marginTop:10,height: 50, borderColor: '#26315F', borderWidth: 1,borderRadius:10,fontSize: 18,paddingHorizontal : 16},
        textlabel:{marginTop:10,fontSize:15,fontFamily:'roboto',fontWeight: "normal",color:'#26315F',fontStyle:'normal'},
        FacebookStyle: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        GooglePlusStyle: {

            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        account: {
            color: '#26315F',
            paddingHorizontal: 17,

        },
        signIn :{
            color: '#00CB99',
        },
        signup : {
            color: '#26315F',
            fontWeight:'bold' ,
            fontSize: 20,
        },
        signtxt : {
            color: '#26315F',
            fontSize: 15,
        },
    }
);
