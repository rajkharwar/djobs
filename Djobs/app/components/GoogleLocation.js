import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Alert,
    TouchableOpacity
} from "react-native";

import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';



Geocoder.init('AIzaSyDVo9Zmn86bAlIMz4pxCqUeDdn0Gm2I4pw',{language : "en"});
export default class App extends Component {
    state = {
        location: null,
        latitude:0,
        longitude: 0,
        Address:null
    };

    findCoordinates = () => {
        Geolocation.getCurrentPosition(
            position => {
                const location = JSON.stringify(position);

                this.setState({latitude:position.coords.latitude,longitude:position.coords.longitude});
               //alert(this.state.latitude);
               alert(location);
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        Geocoder.from(this.state.latitude,this.state.longitude)


            .then(json => {

                console.log(json);


                var addressComponent = json.results[0].address_components;
                this.setState({
                    Address: addressComponent

                })
               // alert(JSON.stringify(this.state.Address));
                console.log(addressComponent);

            })

            .catch(error => console.warn(error));



    };

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.findCoordinates}>
                    <Text style={styles.welcome}>Find My Coords?</Text>

                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5
    }
});