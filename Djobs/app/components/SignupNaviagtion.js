import  Signup  from './Signup';
import EnterOtp from './EnterOtp';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const SignUpNavigation = createStackNavigator(
    {
        Signup: { screen: Signup },
        EnterOtp: { screen: EnterOtp },
    },
    {
        initialRouteName: 'Signup',
        headerMode: 'none'
    }
)
export default SignUpNavigation;