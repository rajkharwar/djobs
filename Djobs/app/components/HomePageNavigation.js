import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const HomePageNavigation = createStackNavigator(
    {
        Signup: { screen: Signup },
        EnterOtp: { screen: EnterOtp },
    },
    {
        initialRouteName: 'Signup',
        headerMode: 'none'
    }
)
export default HomePageNavigation;