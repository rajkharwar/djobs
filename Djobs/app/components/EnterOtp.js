import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Alert, TouchableOpacity} from 'react-native';

import OTPTextView from 'react-native-otp-textinput';

export default class EnterOtp extends Component {
    state = {
        text2: '',

    }

    alertText = () => {
        const {  text2 = '' } = this.state;
        Alert.alert(`${text2}`);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}> Enter Your OTP</Text>
                <Text style={styles.textStyle}>We have sent OTP to your registered mobile number +91 - 7416283027  and email ankit.rajpoot@gmail.com</Text>
                <OTPTextView
                    containerStyle={styles.textInputContainer}
                    handleTextChange={text => this.setState({ text2: text })}
                    textInputStyle={styles.roundedTextInput}
                    inputCount={4}
                    keyboardType="numeric"
                />
                <TouchableOpacity style={styles.otpButton} onPress={this.alertText}>
                    <Text>Verify and Continue</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',


    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        fontSize: 22,
        fontWeight: '500',
        color: '#333333',
        marginBottom: 20,
    },
    textInputContainer: {
        marginBottom: 20,
    },
    roundedTextInput: {
        borderRadius: 10,
        backgroundColor: '#00cc99',

    },
    textStyle: {
        fontSize: 15,
        fontWeight: '500',
        color: '#333333',
        marginBottom: 20,
        paddingHorizontal: 17,
    },
    otpButton:{
        marginTop:10,
        flexDirection:'row',
        height: 50,
        justifyContent: 'center',
        backgroundColor:'#DDDDDD',
        paddingTop:20,
        borderRadius:20,
        width: 300,
    }
});

