import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
} from 'react-native';
export default class WelcomeTwo extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../images/welcomeImg.png')}
                       style={{width: '100%', height: '21%'}}/>
                <Text style={styles.welcomeText}> Easy To get a job </Text>
                <Text style={styles.welcometxt}> Lorem 2ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </Text>
            </View>



        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    welcomeText : {
        marginVertical: 19,
        color :'#00CB99',
        fontSize : 20,
    },
    welcometxt : {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 25,
        color: '#243164',
        fontSize: 15,
    }
});
