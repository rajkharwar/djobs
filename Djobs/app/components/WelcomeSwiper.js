import React, {Component} from 'react';
import { StyleSheet ,View,Text,Image} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

export default class WelcomeSwiper extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDone = () => {
        this.props.navigation.navigate('SignUpNavigation');
    };
    _onSkip = () => {
        this.setState({showRealApp: true});
    };
    _renderItem = ({item}) => {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: item.backgroundColor,
                    alignItems: 'center',
                    justifyContent: 'space-around',
                    paddingBottom: 100
                }}>
                <Text style={styles.title}>{item.title}</Text>
                <Image style={styles.image} source={item.im}/>
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    };

    render() {
        return (
            <AppIntroSlider
                slides={slides}
                renderItem={this._renderItem}
                onDone={this._onDone}
                showSkipButton={true}
                onSkip={this._onSkip}
            />
        );
    }
}
const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 200,
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});

const slides = [
    {
        key: 's1',
        text: 'Best Recharge offers',
        title: 'Mobile Recharge',
        im:require('../images/welcomeImg.png'),
        backgroundColor: 'red',
    },
    {
        key: 's2',
        title: 'Flight Booking',
        text: 'Upto 25% off on Domestic Flights',
        im:require('../images/welcomeImg.png'),
        backgroundColor: 'yellow',
    },
    {
        key: 's3',
        title: 'Great Offers',
        text: 'Enjoy Great offers on our all services',
        im:require('../images/welcomeImg.png'),
        backgroundColor: 'green',
    }
];