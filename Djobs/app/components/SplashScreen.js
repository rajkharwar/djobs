import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,

} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body} from 'native-base';
import Signup from './Signup';
import WelcomeOne from './WelcomeOne';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomePage from './HomePage';
import SignUpNavigation from './SignupNaviagtion';
import SignIn from './SignIn';
import AsyncStorage from '@react-native-community/async-storage';
/*
const initialNavigator = createSwitchNavigator(
    {
        SplashScreen:SplashScreen,
        SignUpNavigation:SignUpNavigation,
        HomePage: HomePage,
        SignIn:SignIn,
    },
    {
        initialRouteName: 'HomePage',
    }

);
const AppContainer = createAppContainer(initialNavigator);*/

export default class SplashScreen extends React.Component {
    performTimeConsumingTask = async() => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                2000
            )
        )
    }

    async componentDidMount() {

        const data = await this.performTimeConsumingTask();

        const isLoggedIn = await AsyncStorage.getItem('isLoggedIn');
        const isnotFirsttime = await AsyncStorage.getItem('isFirstTime');
        if (data !== null) {
            if (isLoggedIn==1) {
                this.props.navigation.navigate('HomePage');
            } else if (isnotFirsttime!=1) {
                this.props.navigation.navigate('WelcomeSwiper');
            } else {
                this.props.navigation.navigate('SignIn');
            }
        }
    }


    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../images/logod.png')}
                       style={{width: '100%', height: '16%'}}/>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#243164',
    }
});
