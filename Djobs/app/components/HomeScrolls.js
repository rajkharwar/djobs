import React ,{Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    FlatList,Image
} from 'react-native';

export default class HomeScrolls extends React.Component{
    render()
    {
        return(
          <View style={{height:130,marginTop:5}}>
               <ScrollView
                   horizontal={true}
                   showsHorizontalScrollIndicator={false}
               >
                   <View style={{height:115,width:140,marginLeft:20,borderWidth:0.5,borderColor:'#fffff',borderRadius:10}}>
                       <View style={{flex:1}}>
                           <Image source={require('../images/recommend.png')}  style={{flex:1,width:null,height:null,resizeMode:'cover'}}></Image>
                       </View>
                       <View style={{flex:1,paddingLeft:10,paddingTop:10}}>
                           <Text>Hondacity</Text>
                           <Text>15000/mon</Text>
                       </View>
                   </View>

                   <View style={{height:115,width:140,marginLeft:20,borderWidth:0.5,borderColor:'#dddddd',borderRadius:10}}>
                       <View style={{flex:1}}>
                           <Image source={require('../images/recommend.png')}  style={{flex:1,width:null,height:null,resizeMode:'cover'}}></Image>
                       </View>
                       <View style={{flex:1,paddingLeft:10,paddingTop:10}}>
                           <Text>Hondacity</Text>
                           <Text>15000/mon</Text>
                       </View>
                   </View>

                   <View style={{height:115,width:140,marginLeft:20,borderWidth:0.5,borderColor:'#dddddd',borderRadius:7}}>
                       <View style={{flex:1}}>
                           <Image source={require('../images/recommend.png')}  style={{flex:1,width:null,height:null,resizeMode:'cover'}}></Image>
                       </View>
                       <View style={{flex:1,paddingLeft:10,paddingTop:10}}>
                           <Text>Hondacity</Text>
                           <Text>15000/mon</Text>
                       </View>
                   </View>

                   <View style={{height:130,width:130,marginLeft:20,borderWidth:0.5,borderColor:'#dddddd',borderRadius:7}}>
                       <View style={{flex:1}}>
                           <Image source={require('../images/recommend.png')}  style={{flex:1,width:null,height:null,resizeMode:'cover'}}></Image>
                       </View>
                       <View style={{flex:1,paddingLeft:10,paddingTop:10}}>
                           <Text>Hondacity</Text>
                           <Text>15000/mon</Text>
                       </View>
                   </View>
               </ScrollView>
          </View>
        );
    }

}