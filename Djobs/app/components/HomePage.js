import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    SafeAreaView,
    FlatList,Image
} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body} from 'native-base';
import Carousel from "react-native-looped-carousel-improved";

import {Dimensions} from 'react-native';
import HomeScrolls from './HomeScrolls';
const DeviceWidth = Dimensions.get('window').width

const { width, height } = Dimensions.get("window");

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
    this.state = {
        size: { width, height }
    };
}
    _onLayoutDidChange = e => {
        const layout = e.nativeEvent.layout;
        this.setState({
            size: { width: layout.width, height: layout.height}
        });
    };
    render() {
        return (
            <View>
                <View style={{flexDirection:'row',paddingLeft:10,alignItems:'center',marginHorizontal:18,height:64,width:DeviceWidth-30}}>
                    <View style={{flexDirection: 'column'}}>
                    <Image
                        source={require('../images/Oval.png')}
                        style={{ width:10,height:10}}
                    />
                    <Image
                        source={require('../images/location.png')}
                        style={{ width:0.50,height:15,paddingLeft:8}}
                    />
                    </View>
                    <View style={{flexDirection: 'column'}}>
                        <Text style={{fontSize:10}}> You are here</Text>

                        <Text style={{fontSize:15,fontFamily:'Gilroy'}}>BTM second stage</Text>
                    </View>
                </View>
                <View onLayout={this._onLayoutDidChange} style={{marginLeft:10,padding:10,borderWidth:2,borderColor:'#f2f2f2'}}>
                    <Carousel
                        delay={4000}
                        style={{
                            width: DeviceWidth,
                            height: (DeviceWidth * 9) / 25,
                        }}
                        autoplay
                        isLooped
                    >

                           <Image
                            source={require('../images/mainpagecrousal.png')}
                            style={{ width: width-30,aspectRatio: 16 / 7,resizeMode: "stretch"}}
                        />

                        <Image
                            source={require('../images/mainpage2.png')}
                            style={{ width: width-30, aspectRatio: 16 / 7, resizeMode: "stretch" }}
                        />

                    </Carousel>
                </View>

                <View style={styles.container} >
                 <View style={styles.box1}>
                     <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Full time</Text>
                 </View>
                 <View style={styles.box2}>
                     <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Monthly</Text>
                 </View>
                 <View style={styles.box3}>
                     <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Part Time</Text>
                 </View>
                  <View style={styles.box4}>
                      <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Weekly</Text>
                  </View>
                  <View style={styles.box5}>
                      <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Hourly</Text>
                  </View>
                  <View style={styles.box6}>
                      <Text style={{textAlign: 'center',marginTop: 15}}> <Text style={{fontSize: 25,fontWeight: 'bold'}}>200</Text><Text>jobs</Text></Text><Text style={{marginTop:5,textAlign: 'center'}}>Trip</Text>
                  </View>
               </View>
                <HomeScrolls/>
                <View style={{flexDirection:'row',paddingLeft:15,alignItems:'center',marginHorizontal:18,backgroundColor:'#fedddc',height:64,width:DeviceWidth-30}}>
                    <Image
                        source={require('../images/Path.png')}
                        style={{ width:40,height:40}}
                    />
                    <View style={{flexDirection: 'column'}}>
                    <Text style={{fontSize:15}}> Working Status</Text>

                     <Text style={{fontSize:12}}>Hi Robert currently you are working with Peter</Text>
                    </View>
                </View>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginTop:10,
        height: DeviceWidth*0.5,
        flexDirection: "row",
        backgroundColor: "#fff",
        flexWrap: "wrap",
        alignItems: "flex-start",
        justifyContent: "space-evenly"
    },
    box1: {
        width:DeviceWidth*0.28,
        height: DeviceWidth*0.20,
        backgroundColor: "#00cc99",
        borderRadius: 6,
        margin:10,

    },
    box2: {
        width: DeviceWidth*0.28,
        height:DeviceWidth*0.20,
        backgroundColor: "#ff7474",
        borderRadius: 6,
        margin:10
    },
    box3: {
        width: DeviceWidth*0.28,
        height:DeviceWidth*0.20,
        backgroundColor: "#5cb7f8",
        borderRadius: 6,
        margin:10
    },
    box4: {
        width: DeviceWidth*0.28,
        height:DeviceWidth*0.20,
        backgroundColor: "#fac54b",
        borderRadius: 6,
        margin:10
    },
    box5: {
        width: DeviceWidth*0.28,
        height:DeviceWidth*0.20,
        backgroundColor: "#923cb4",
        borderRadius: 6,
        margin:10
    },
    box6: {
        width: DeviceWidth*0.28,
        height:DeviceWidth*0.20,
        backgroundColor: "#adbe52",
        borderRadius: 6,
        margin:10
    }

});
