import React from 'react';
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Image, ScrollView} from 'react-native';
 import ValidationComponent from 'react-native-form-validator/index';

import {
  Card,
  Button,
  Form,
  Input,
  Label,
  Header,
  Item,
  Content,
  Container,
  CardItem,
} from 'native-base';
import axios from 'axios';

'use strict';
class Signup extends ValidationComponent {
  constructor(props) {
        super(props);
      this.state = {firstName : "", lastName: " ", mobileNumber:" ",email:""};
    }

  /*  _getresponse=()=> {
        // const formData = new FormData();
        //formData.append("data", true);
        /* axios.post('https://djobs.azurewebsites.net/api/Login/SignUp', {
              FirstName: this.state.FirstName,
              LastName: this.state.lastName,
              Email:this.state.email,
              ClientTypeId:1,
             // Mobile:this.state.mobileNumber
             Mobile:'888877777'
          })
              .then(function (response) {
                  alert(response.data);
                  //alert(response.data);

                 // return response.data;
              })
              .catch(function (error) {
                  alert("error");
                  console.log(error);
              });
      // alert(res.FirstName);
       //return res;
      }*/

        _onPressButton = () => {
            /* //const datafromserver =
               this._getresponse();
            /* if(datafromserver=="EXIST")
             {

             }
             else
             {
                let abc= datafromserver.FirstName
                //alert(abc);
             }
       */
           // alert("called");
            axios.post('https://djobs.azurewebsites.net/api/Login/SignUp', {
                FirstName: this.state.firstName,
                LastName: this.state.lastName,
                Email: this.state.email,
                ClientTypeId: 1,
                Mobile: this.state.mobileNumber
            })
                .then((response) => {

                    alert(response.data.FirstName);
                }, (error) => {
                    console.log(error);
                    alert("error");
                });
        }

  render() {
    return (
        <ScrollView style={styles.scrollView}>
        <View style={{paddingTop:10,paddingHorizontal:18,flex: 1}}>
            <Text style={styles.signup}>Sign Up</Text>
            <Text style={styles.signtxt}>Find the job on Djobs. You are only few steps away from bunch of jobs</Text>
            <Text style={styles.textlabel}>First Name</Text>
        <TextInput style={styles.inputtext} onChangeText={(firstName) => this.setState({firstName})} value={this.state.firstName}  placeholder = "Type Here.." underlineColorAndroid='transparent'/>
            <Text style={styles.textlabel}>Last Name</Text>
        <TextInput style={styles.inputtext}  onChangeText={(lastName) => this.setState({lastName})} value={this.state.lastName}  pattern="[1-9]{1}[0-9]{9}"  placeholder = "Type Here.." underlineColorAndroid='transparent'/>
            <Text style={styles.textlabel}>Mobile Number</Text>
        <TextInput style={styles.inputtext}  onChangeText={(mobileNumber) => this.setState({mobileNumber})} value={this.state.mobileNumber} placeholder = "Type Here.." underlineColorAndroid='transparent'/>
            <Text style={styles.textlabel}>Email</Text>
        <TextInput style={styles.inputtext}  onChangeText={(email) => this.setState({email})} value={this.state.email} placeholder = "Type Here.." underlineColorAndroid='transparent'/>
            <TouchableOpacity onPress={this._onPressButton} style={{marginTop:10,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#DDDDDD',paddingTop:20,borderRadius:10}}>
            <Text>Send OTP</Text>
        </TouchableOpacity>
            <View style={{justifyContent:'center',height:'10%',alignItems:'center'}}>
                <Text style={styles.signtxt}>or Continue with a social Account</Text>
            </View>
            <View style={{justifyContent:'space-between',flexDirection: 'column'}}>
                <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5}>
                <Image
                    source={require('../images/fbLogin.png')}
                /><View style={{flexDirection:'row'}}>
                    <Text style={styles.account}>Already have an account? </Text>

                    <TouchableOpacity style={{height: 50}}><Text style={styles.signIn}>Sign In</Text></TouchableOpacity>

                </View>
            </TouchableOpacity>
                <TouchableOpacity style={styles.GooglePlusStyle} activeOpacity={0.5}>
                    <Image
                        source={require('../images/googleLogin.png')}

                    />
                </TouchableOpacity>


            </View>


                <Text style={styles.signtxt}>By creating an account you agree to our Terms of Use and Privacy Policy</Text>

        </View>
        </ScrollView>
    );
  }
}
const styles= StyleSheet.create(
    {

        inputtext:{marginTop:10,height: 50, borderColor: '#26315F', borderWidth: 1,borderRadius:10,fontSize: 18,paddingHorizontal : 16},
        textlabel:{marginTop:10,fontSize:15,fontFamily:'roboto',fontWeight: "normal",color:'#26315F',fontStyle:'normal'},
        FacebookStyle: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        GooglePlusStyle: {

            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        account: {
            color: '#26315F',
            paddingHorizontal: 17,

        },
        signIn :{
            color: '#00CB99',
        },
        signup : {
            color: '#26315F',
            fontWeight:'bold' ,
            fontSize: 20,
        },
        signtxt : {
            color: '#26315F',
            fontSize: 15,
        },
    }
);
export default Signup;
