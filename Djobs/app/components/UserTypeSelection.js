import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image} from 'react-native';
import ValidationComponent from "react-native-form-validator";


export default class UserTypeSelection extends React.Component {
    constructor(props) {
        super(props);
    }
        state = {
            names: [
                {
                    id: 0,
                    name: 'Driver',
                    texts: 'If you are a driver and looking for a job and extra income.',


                },
                {
                    id: 1,
                    name: 'Car Owner',
                    texts: 'If you have a car and want to earn extra hire a drive or rent your car.',

                },
                {
                    id: 2,
                    name: 'Leasing Firm',
                    texts: 'If you are a driver and looking for a job and extra income.',

                },

            ]
        }




    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}> Choose Your Profile</Text>
                <Text style={styles.textStyle}>Lorem Ispum Although we try to do everything, there are some things we cannot do!</Text>
                <View>
                    {
                        this.state.names.map((item, index) => (
                            <Text style = {styles.containerText}>
                                <Text style = {styles.text}>

                                {item.name}

                                    {"\n"}
                            </Text>
                                <Text>
                                    {item.texts}
                                </Text>

                            </Text>
                        ))
                    }
                </View>



            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop:12,
        paddingHorizontal:13,
        flex:1,
        backgroundColor: '#1F253C',
    },
    instructions: {
        fontSize: 22,
        fontWeight: '600',
        color: '#FFFFFF',
        marginBottom: 20,
    },
    textStyle: {
        fontSize: 15,
        fontWeight: '500',
        color: '#ffffff',
        marginBottom: 20,

    },
    containerText: {
        padding: 10,
        marginTop: 13,
        backgroundColor: '#ffffff',
        height: 100,
        borderWidth: 1,
        borderRadius:10,
        paddingHorizontal : 16,


    },
    text: {
        color: '#464646',
        fontWeight: 'bold',
        fontSize:20,
        alignItems: 'center',

    },
    imageStyle: {
        height: 100,
        width: 50,
    }

});


