import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native';
import axios from 'axios';
export default class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = {Password : "", LoginId : ""};
    }
    _onPressButton=()=> {
       // alert(this.state.LoginId+' '+this.state.Password);
            axios.get('https://djobs.azurewebsites.net/api/Login/SignIn?LoginId='+this.state.LoginId+'&Password='+this.state.Password)
                .then((response) => {
                    alert(response.data);
                }, (error) => {
                    console.log(error);
                    alert(error);
                });
    }

    render() {
        return (
            <View style={{paddingTop: 10, paddingHorizontal: 18, flex: 1}}>
                <Text style={styles.signup}>Sign In</Text>
                <Text style={styles.signtxt}>Find the job on Djobs. You are only few steps away from bunch of
                    jobs</Text>
                <Text style={styles.textlabel}>Email/Mobile Number</Text>
                <TextInput style={styles.inputtext}
                           onChangeText={(LoginId) => this.setState({LoginId})}
                           value={this.state.LoginId} placeholder="Enter Email/Mobile Number"
                           underlineColorAndroid='transparent'/>

                <Text style={styles.textlabel}>Password</Text>
                <TextInput style={styles.inputtext} secureTextEntry={true} onChangeText={(Password) => this.setState({Password})}
                           value={this.state.Password} placeholder="Enter Password"
                           underlineColorAndroid='transparent'/>

                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={{height: 50}}><Text style={styles.Forgot}>Forgot Password?</Text></TouchableOpacity>
                </View>
                <TouchableOpacity onPress={this._onPressButton} style={{
                    marginTop: 100,
                    flexDirection: 'row',
                    height: 50,
                    justifyContent: 'center',
                    backgroundColor: '#DDDDDD',
                    paddingTop: 20,
                    borderRadius: 10,
                }}>
                    <Text>Sign In</Text>
                </TouchableOpacity>

                <View style={{justifyContent:'center',height:'10%',alignItems:'center'}}>
                    <Text style={styles.signtxt}>or Continue with a social Account</Text>
                </View>
                <View style={{justifyContent:'space-between',flexDirection: 'column'}}>
                    <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5}>
                        <Image
                            source={require('../images/fbLogin.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.GooglePlusStyle} activeOpacity={0.5}>
                        <Image
                            source={require('../images/googleLogin.png')}

                        />
                    </TouchableOpacity>

                </View>
                <View style={{flexDirection:'row'}}>
                    <Text style={styles.account}>Don't have an account? </Text>
                    <TouchableOpacity style={{height: 50}}><Text style={styles.signIn}>Sign Up</Text></TouchableOpacity>
                </View>

                {/*<Text style={styles.signtxt}>By creating an account you agree to our Terms of Use and Privacy Policy</Text>*/}

            </View>

        );
    }
}

const styles= StyleSheet.create(
    {
        inputtext:{marginTop:10,height: 50, borderColor: '#26315F', borderWidth: 1,borderRadius:10,fontSize: 18,paddingHorizontal : 16},
        textlabel:{
            marginTop:10,
            fontSize:15,
            fontFamily:'roboto',
            fontWeight: "normal",
            color:'#26315F',
            fontStyle:'normal',
            paddingVertical: 5,
        },
        FacebookStyle: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        GooglePlusStyle: {

            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 17,
            height: 60,
            width: 100,
        },
        account: {
            color: '#26315F',
            paddingHorizontal: 17,

        },
        signIn :{
            color: '#00CB99',
        },
        Forgot :{
            color: '#e41528',
            fontSize: 15,
            paddingLeft:230,

        },
        signup : {
            color: '#26315F',
            fontWeight:'bold' ,
            fontSize: 20,
            paddingVertical:25,
        },
        signtxt : {
            color: '#26315F',
            fontSize: 15,
        },
    }
);
