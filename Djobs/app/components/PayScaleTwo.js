import React, { Component } from 'react';
import {
    Text,
    TextInput,
    Picker,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
} from 'react-native';
import axios from 'axios';
export default class PayScaleTwo extends Component {

    constructor(props) {
        super(props);
        this.state = {Password : "", LoginId : ""};
    }
    _onPressButton=()=> {
        // alert(this.state.LoginId+' '+this.state.Password);
        axios.get('https://djobs.azurewebsites.net/api/Login/SignIn?LoginId='+this.state.LoginId+'&Password='+this.state.Password)
            .then((response) => {
                alert(response.data);
            }, (error) => {
                console.log(error);
                alert(error);
            });
    }

    render() {
        return (
            <ScrollView style={styles.scrollView}>
            <View style={{paddingTop: 10, paddingHorizontal: 18, flex: 1}}>
                <TouchableOpacity activeOpacity={0.5}>
                    <Image
                        source={require('../images/arrowBack.png')}
                    />
                </TouchableOpacity>
                <Text style={styles.payScale}>Pay Scale</Text>
                <Text style={styles.paytxt}>Lorem Ipsum is simply dummy text of the printing and type setting industry.</Text>

                <View style={styles.PayBox}>
                    <View style={{justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            height: 50,
                            justifyContent: 'center',
                            backgroundColor: '#F1F2F6',
                            paddingTop: 20,
                            width:70,
                            borderRadius: 10,
                            color:'#26315F',
                        }}>
                            <Text>HOURLY</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            height: 50,
                            justifyContent: 'center',
                            backgroundColor: '#F1F2F6',
                            paddingTop: 20,
                            width:70,
                            borderRadius: 10,
                            color:'#26315F',

                        }}>
                            <Text>WEEKLY</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            height: 50,
                            justifyContent: 'center',
                            backgroundColor: '#FF7474',
                            paddingTop: 20,
                            width:70,
                            borderRadius: 10,

                        }}>
                            <Text style={{color:'#ffffff',}}>MONTHLY</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            height: 50,
                            justifyContent: 'center',
                            backgroundColor: '#F1F2F6',
                            paddingTop: 20,
                            width:70,
                            borderRadius: 10,
                            color:'#26315F',
                        }}>
                            <Text>KM</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            height: 50,
                            width:70,
                            justifyContent: 'center',
                            backgroundColor: '#F1F2F6',
                            paddingTop: 20,
                            borderRadius: 10,
                            color:'#26315F',
                        }}>
                            <Text>TRIP</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={styles.textlabel}>Driver Description</Text>
                <Picker
                    selectedValue={this.state.language}
                    style={{height: 50, width: 100}}
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({language: itemValue})
                    }>
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                </Picker>
                <Text style={styles.textlabel}>Monthly Pricing</Text>
                <TextInput style={styles.inputtext}
                           placeholder="eg., Rs 12000"
                           underlineColorAndroid='transparent'/>
                <Text style={styles.textlabel}>Holidays</Text>
                <TextInput style={styles.inputtext}
                           placeholder="eg. 350 rs per hour"
                           underlineColorAndroid='transparent'/>
                <Text style={styles.textlabel}>Extra Hour</Text>
                <TextInput style={styles.inputtext}
                           placeholder="eg. 350 rs per hour"
                           underlineColorAndroid='transparent'/>
                <Text style={styles.textlabel}>Night Charges (Hour)</Text>
                <TextInput style={styles.inputtext}
                           placeholder="eg. 350 rs per hour"
                           underlineColorAndroid='transparent'/>

                <View style={{flexDirection:'row'}}>

                    <TouchableOpacity  style={{height: 50}}><Text style={styles.Description}> <Image source={require('../images/plus.png')}/> Add more Description </Text></TouchableOpacity>
                </View>
                <View style={{justifyContent:'space-around',paddingHorizontal: 20}}>
                    <TouchableOpacity onPress={this._onPressButton} style={{marginBottom:2,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#CDCDCD',paddingTop:20,borderRadius:20}}>
                        <Text style={{color:'#ffffff'}}>Save & Next</Text>
                        <Image
                            source={require('../images/arrow.png')}
                        />
                    </TouchableOpacity>
                </View>

            </View>
            </ScrollView>

        );
    }
}

const styles= StyleSheet.create(
    {
        PayBox: {
            flexDirection: 'row',
            justifyContent:'space-between',


        },
        textPro : {
            color: '#ffffff',
            fontWeight:'bold' ,
            fontSize: 20,
            paddingTop: 10,
            paddingHorizontal:29,
        },


        inputtext:{height: 50,justifyContent: 'center',backgroundColor:'#F1F2F6',paddingTop:20,borderRadius:15,paddingHorizontal : 16},
        textlabel:{
            fontSize:15,
            fontFamily:'roboto',
            fontWeight: "normal",
            color:'#29275F',
            fontStyle:'normal',
            paddingVertical: 5,
        },

        account: {
            color: '#26315F',
            paddingHorizontal: 17,

        },
        payScale : {
            color: '#26315F',
            fontWeight:'bold' ,
            fontSize: 20,
            // paddingVertical:10,
        },
        paytxt : {
            color: '#26315F',
            fontSize: 15,
        },
        Description :{
            color: '#FF7474',
            fontSize: 15,
            paddingTop:10,


        },
    }
);
