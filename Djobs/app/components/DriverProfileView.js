import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image} from 'react-native';
import ValidationComponent from "react-native-form-validator";


export default class DriverProfileView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <View style={{flexDirection:'column',backgroundColor: '#1F253C'}}>
                <Text style={styles.textPro}>Profile Details</Text>
                <View style={styles.container}>

                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <View style={styles.viewStyleOne}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:50,width:100,paddingVertical:50,marginBottom:15}}

                                       source={require('../images/resume.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}> Personal Detail</Text>
                        </View>
                        <View style={styles.viewStyleTwo}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:100,width:100,marginBottom:15}}
                                       source={require('../images/calendar.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}> Work Schedule </Text>
                        </View>
                        <View style={styles.viewStyleThree}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:40,width:75,paddingVertical:50,marginBottom:15}}
                                       source={require('../images/contract.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}>Bank Detail</Text>

                        </View>
                    </View>


                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <View style={styles.viewStyleOne}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:50,width:95,paddingVertical:50,marginBottom:15}}

                                       source={require('../images/experience.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}>Expriences / DL </Text>
                        </View>
                        <View style={styles.viewStyleTwo}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:100,width:100,marginBottom:15}}
                                       source={require('../images/payment.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}>Pay Scale </Text>
                        </View>
                        <View style={styles.viewStyleThree}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Image style={{height:60,width:95,marginBottom:40}}
                                       source={require('../images/driving.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.textStyle}>Vechile Prefrence </Text>

                        </View>
                    </View>


                    </View>
                <View style={{justifyContent:'space-around',paddingHorizontal: 20,paddingTop:10}}>
                    <TouchableOpacity onPress={this._onPressButton} style={{marginBottom:23,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#00CB99',paddingTop:20,borderRadius:20}}>
                        <Text style={{color:'#ffffff'}}>Okay, got it</Text>
                        <Image
                            source={require('../images/arrow.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>





        );

    }
}
//Styles
const styles = StyleSheet.create({
    container: {
        paddingTop:10,
        paddingHorizontal:16,
       // flex:1,
        height:550,
        justifyContent:'space-around',
        flexDirection: 'row',

       },
    textPro : {
        color: '#ffffff',
        fontWeight:'bold' ,
        fontSize: 20,
        paddingTop: 10,
        paddingHorizontal:29,
    },

    viewStyleOne: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,


    },
    viewStyleTwo: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,

    },
    viewStyleThree: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,
    },
    textStyle:{
        textAlign:'center'
    },

    iconStyle: {
        alignItems: 'center',
       // height: 15,
        //width: 10,
    },
    instructions: {
        fontSize: 22,
        fontWeight: '600',
        color: '#FFFFFF',
        marginBottom: 20,
    },
})




