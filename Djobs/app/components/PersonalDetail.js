import React, { Component } from 'react';
import {
    Text,
    TextInput,
    Picker,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
} from 'react-native';
import axios from 'axios';
export default class PersonalDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {Password : "", LoginId : ""};
    }
    _onPressButton=()=> {
        // alert(this.state.LoginId+' '+this.state.Password);
        axios.get('https://djobs.azurewebsites.net/api/Login/SignIn?LoginId='+this.state.LoginId+'&Password='+this.state.Password)
            .then((response) => {
                alert(response.data);
            }, (error) => {
                console.log(error);
                alert(error);
            });
    }

    render() {
        return (
            <ScrollView style={styles.scrollView}>
                <View style={{paddingTop: 10, paddingHorizontal: 18, flex: 1}}>
                    <TouchableOpacity activeOpacity={0.5}>
                        <Image
                            source={require('../images/arrowBack.png')}
                        />
                    </TouchableOpacity>
                    <Text style={styles.payScale}>Personal Details</Text>
                    <Text style={styles.paytxt}>Lorem Ipsum is simply dummy text of the printing and type setting industry.</Text>

                    <Text style={styles.textlabel}>First Name</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Last Name</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Mobile Number</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Location</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Enter Address..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Address Line 1</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Landmark</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>City</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Pincode</Text>
                    <TextInput style={styles.inputtext}
                               placeholder="Type here..."
                               underlineColorAndroid='transparent'/>
                    <Text style={styles.textlabel}>Instruction</Text>
                    <TextInput style={styles.inputinstuction}
                               placeholder="Type Instruction..."
                               underlineColorAndroid='transparent'/>



                    <View style={{justifyContent:'space-around',paddingHorizontal: 20,paddingBottom:20, paddingTop:15,}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{marginBottom:2,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#CDCDCD',paddingTop:20,borderRadius:20}}>
                            <Text style={{color:'#ffffff'}}>Save & Next</Text>
                            <Image
                                source={require('../images/arrow.png')}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>

        );
    }
}

const styles= StyleSheet.create(
    {
        textPro : {
            color: '#ffffff',
            fontWeight:'bold' ,
            fontSize: 20,
            paddingTop: 10,
            paddingHorizontal:29,
        },


        inputtext:{height: 50,justifyContent: 'center',backgroundColor:'#F1F2F6',paddingTop:20,borderRadius:15,paddingHorizontal : 16},
        textlabel:{
            fontSize:15,
            fontFamily:'roboto',
            fontWeight: "normal",
            color:'#29275F',
            fontStyle:'normal',
            paddingVertical: 5,
        },
        inputinstuction:{
            height: 100,
            justifyContent: 'center',
            backgroundColor:'#F1F2F6',
            paddingTop:20,
            borderRadius:15,
            paddingHorizontal : 16,
        },

        account: {
            color: '#26315F',
            paddingHorizontal: 17,

        },
        payScale : {
            color: '#26315F',
            fontWeight:'bold' ,
            fontSize: 20,
            // paddingVertical:10,
        },
        paytxt : {
            color: '#26315F',
            fontSize: 15,
        },
        Description :{
            color: '#FF7474',
            fontSize: 15,
            paddingTop:10,


        },
    }
);
